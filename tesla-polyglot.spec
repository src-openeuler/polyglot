#Basic Information
Name:           tesla-polyglot
Version:        0.4.0
Release:        1
Summary:        Modules to enable Maven usage in other JVM languages
License:        EPL-1.0
URL:            https://github.com/takari/maven-polyglot
Source0:        https://github.com/takari/polyglot-maven/archive/polyglot-%{version}.tar.gz
Source1:        eclipse-1.0.txt
Patch0:         polyglot-snakeyaml-1.25.patch
BuildArch:      noarch

#Dependency
BuildRequires:  dos2unix maven-local
BuildRequires:  mvn(io.takari.maven.plugins:takari-lifecycle-plugin)
BuildRequires:  mvn(io.takari:takari:pom:)
BuildRequires:  mvn(junit:junit)
BuildRequires:  mvn(org.apache.maven:maven-core)
BuildRequires:  mvn(org.apache.maven:maven-model)
BuildRequires:  mvn(org.apache.maven:maven-model-builder)
BuildRequires:  mvn(org.apache.maven:maven-parent:pom:)
BuildRequires:  mvn(org.apache.maven:maven-plugin-api)
BuildRequires:  mvn(org.apache.maven.plugin-tools:maven-plugin-annotations)
BuildRequires:  mvn(org.codehaus.gmavenplus:gmavenplus-plugin)
BuildRequires:  mvn(org.codehaus.groovy:groovy)
BuildRequires:  mvn(org.codehaus.plexus:plexus-component-annotations)
BuildRequires:  mvn(org.codehaus.plexus:plexus-component-metadata)
BuildRequires:  mvn(org.codehaus.plexus:plexus-utils)
BuildRequires:  mvn(org.eclipse.sisu:org.eclipse.sisu.plexus)
BuildRequires:  mvn(org.slf4j:slf4j-api)
BuildRequires:  mvn(org.yaml:snakeyaml)
Buildrequires:  mvn(commons-beanutils:commons-beanutils)
Buildrequires:  mvn(org.apache.maven.plugins:maven-invoker-plugin)

Obsoletes:     %{name}-cli
Obsoletes:     %{name}-clojure

%description
Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows the POM
model to be written in dialects other than XML. Several of the dialects also
allow inlined plugins: the Ruby, Groovy and Scala dialects allow this.

%package atom
Summary:       Polyglot Tesla :: Atom

%description atom
Polyglot Tesla :: Atom.

%package common
Summary:       Polyglot Tesla :: Common

%description common
Polyglot Tesla :: Common.

%package groovy
Summary:       Polyglot Tesla :: Groovy

%description groovy
Polyglot Tesla :: Groovy.

%package maven-plugin
Summary:       Polyglot Tesla :: Maven Plugin

%description maven-plugin
This package contains Polyglot Tesla Maven Plugin.

%package translate-plugin
Summary:       Polyglot :: Translate Plugin

%description translate-plugin
This package contains Polyglot Translate Plugin.

%package xml
Summary:       Polyglot Tesla :: XML

%description xml
Polyglot Tesla :: XML.

%package yaml
Summary:       Polyglot Tesla :: YAML

%description yaml
Polyglot Tesla :: YAML.

%package javadoc
Summary:       Javadoc for %{name}

%description javadoc
This package contains javadoc for %{name}.

#Build sections
%prep
%autosetup -n polyglot-maven-polyglot-%{version} -p1

find -name "*.class" -delete
find -name "*.jar" -delete
%pom_remove_plugin org.codehaus.plexus:plexus-component-metadata

# Unavailable build deps/tools
%pom_disable_module polyglot-clojure
%pom_disable_module polyglot-scala
%pom_remove_dep :polyglot-scala polyglot-translate-plugin
%pom_disable_module polyglot-ruby
%pom_remove_dep -r :polyglot-ruby
%pom_disable_module polyglot-kotlin
%pom_remove_dep -r :polyglot-kotlin
%pom_disable_module polyglot-java
%pom_remove_dep -r :polyglot-java

%pom_remove_dep rubygems:maven-tools polyglot-ruby
rm -Rf polyglot-ruby/src/{test,it}
%pom_remove_plugin :maven-invoker-plugin polyglot-ruby

# Unavailable plugin
%pom_remove_plugin org.codehaus.groovy:groovy-eclipse-compiler polyglot-groovy
%pom_remove_dep org.codehaus.groovy:groovy-eclipse-batch polyglot-groovy
%pom_remove_dep org.codehaus.groovy:groovy-eclipse-compiler polyglot-groovy
%pom_remove_plugin :maven-compiler-plugin polyglot-groovy
# use gmavenplus
%pom_add_plugin org.codehaus.gmavenplus:gmavenplus-plugin:2.1 polyglot-groovy "
 <executions>
  <execution>
   <goals>
    <!--goal>generateStubs</goal-->
    <goal>generateTestStubs</goal>
    <!--goal>compile</goal>
    <goal>testCompile</goal-->
   </goals>
  </execution>
 </executions>"

for p in maven-plugin translate-plugin; do
  %pom_add_plugin "org.apache.maven.plugins:maven-plugin-plugin:3.4" polyglot-${p} "
  <configuration>
    <skipErrorNoDescriptorsFound>true</skipErrorNoDescriptorsFound>
  </configuration>"
%pom_xpath_inject "pom:dependency[pom:groupId = 'org.apache.maven']" "<version>3.3.1</version>" polyglot-${p}
done

%pom_xpath_inject "pom:project/pom:dependencies/pom:dependency[pom:groupId = 'org.apache.maven']" '<version>${mavenVersion}</version>'

# atom common maven-plugin translate-plugin
# diamond operator
for m in yaml groovy; do
%pom_add_plugin org.apache.maven.plugins:maven-compiler-plugin:3.0 polyglot-${m} '
<configuration>
 <source>1.7</source>
 <target>1.7</target>
 <encoding>UTF-8</encoding>
</configuration>'
done

# Use web access
sed -i '/pyyaml/d' polyglot-yaml/src/test/java/org/sonatype/maven/polyglot/yaml/CompactFormatTest.java

# test skipped for unavailable dependency org.easytesting:fest-assert:1.1
rm -rf polyglot-clojure/src/test/java/*

# com.cedarsoftware:java-util:1.19.3
sed -i '/DeepEquals/d' polyglot-xml/src/test/java/org/sonatype/maven/polyglot/xml/TestReaderComparedToDefault.java
%pom_remove_dep com.cedarsoftware:java-util polyglot-xml

# ComparisonFailure
rm polyglot-yaml/src/test/java/org/sonatype/maven/polyglot/yaml/SnakeYamlModelReaderTest.java

cp -p %{SOURCE1} .
dos2unix eclipse-1.0.txt

%mvn_alias ':polyglot-{*}' io.tesla.polyglot:tesla-polyglot-@1
%mvn_alias ':polyglot-{*}' org.sonatype.pmaven:pmaven-@1

%build
%mvn_build -s -f -- -Dproject.build.sourceEncoding=UTF-8

%install
%mvn_install

#Files list
%files -f .mfiles-polyglot
%doc poms
%license eclipse-1.0.txt license-header.txt

%files atom -f .mfiles-polyglot-atom

%files common -f .mfiles-polyglot-common

%files groovy -f .mfiles-polyglot-groovy

%files maven-plugin -f .mfiles-polyglot-maven-plugin

%files translate-plugin -f .mfiles-polyglot-translate-plugin

%files xml -f .mfiles-polyglot-xml
%doc polyglot-xml/README.md

%files yaml -f .mfiles-polyglot-yaml

%files javadoc -f .mfiles-javadoc
%license eclipse-1.0.txt license-header.txt

%changelog
* Mon Mar 03 2025 Ge Wang <wang__ge@126.com> - 0.4.0-1
- Update to version 0.4.0

* Mon Jul 01 2024 Ge Wang <wang__ge@126.com> - 0.2.1-6
- Fix build failure for gmavenplus-plugin 2.1.0

* Thu Mar 09 2023 yaoxin <yaoxin30@h-partners.com> - 0.2.1-5
- Fix build failure due to snakeyaml update to 1.32

* Mon May 9 2022 baizhonggui <baizhonggui@h-partners.com> - 0.2.1-4
- modify license identifier

* Fri Dec 06 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.2.1-3
- Package init

